const events = require("events");
const net = require("net");

const codeToReason = (code) => {
  const reasons = {
    '200': 'OK',
    '202': 'Accepted ',
    '404': 'Not Found',
    '400': 'Bad Request',
    '500': 'Internal Server Error'
  }
  return reasons[code];
};

const objectToLines = (obj) => {
  return Object.entries(obj)
    .map(pair => `${pair[0]}: ${pair[1]}`)
    .join('\r\n')
}

const composeHttpResponse = (statusCode, headers, body) => {
  //Debe ir en una sola línea porque sino inserta \n
  return `HTTP/1.1 ${statusCode} ${codeToReason(statusCode)}\r\n${objectToLines(headers)}\r\n\r\n${body}\r\n`;
};

//Dividir los headers en key-value
const splitHeaders = (headers) => {
  let data = {};
  headers.split("\r\n").forEach(header => {
    let parts = header.split(": ");
    data[parts[0].toUpperCase()] = parts[1];
  });
  return data;
}

const createServer = (requestHandler) => {

  const server = net.createServer((socket) => {
    let buffer = [];
    let parsed = {};
    let request = "";
    socket.on("data", (data) => {
      //Ejemplo de Request
      //POST /user HTTP/1.1\r\n
      //Content-Type: text/plain\r\n
      //User-Agent: PostmanRuntime/7.26.5\r\n
      //Accept: */*\r\n
      //Postman-Token: b4584a65-ac4b-4e86-ae27-e20a42f85e8b\r\n
      //Host: localhost:8080\r\n
      //Accept-Encoding: gzip, deflate, br\r\n
      //Connection: keep-alive\r\n
      //Content-Length: 4\r\n
      //\r\n
      //hola"
      
      //Obtener un header
      parsed.getHeader = (header) => parsed.headers[header.toUpperCase()];
      //boolean para identificar si la petición viene segmentada
      let incompleto = false;
      //Adjuntar el trozo de petición actual a la anterior en caso que llegue segmentada
      request += data.toString();

      //Verificar que empiece con la primer línea de un http request para saber si es la primera parte de 
      //la request (por si llegara segmentada)
      const regexMatch = /[a-zA-Z]+ \/[ a-zA-Z\/]* HTTP\/1\.1/i.test(request);
      if (regexMatch) {
        //separar headers del body
        headers_body = request.indexOf('\n\r');
        //obtener metodo http
        parsed.method = request.split(" ")[0];
        //obtener ruta
        parsed.path = request.split(" ")[1];

        //Toda la petición menos el body y la linea en blanco
        //los /r o /n se toman como 1 solo caracter
        parsed.headers = splitHeaders(request.substring(request.indexOf('\r\n') + 2, headers_body - 1));
        headers_body += 3;
        //obtener el content-length
        const contentLength = parsed.getHeader('CONTENT-LENGTH');
        //si el content length existe
        if (contentLength != null) {
          //del inicio del body hasta el final segun su content length
          parsed.body = request.substring(headers_body, headers_body + parseInt(contentLength));
          //si la longitud del body es menor que el content-length significa que la petición está segmentada
          //y cambiamos el boolean a true
          if (parsed.body.length < contentLength) {            
            parsed.incompleto = true;
            return;
          }
          //si el content length no existe 
        } else {
          parsed.body = request.substring(headers_body, request.length);
        }
        //si no es la primera parte de la petición http, se concatena al segmento anterior
      } else if (incompleto) {

        request += request;
        return;
      }
      let parsedRequest = parsed;

      requestHandler(parsedRequest, {
        send: (statusCode, headers, body) => {
          //calculamos el content-length
          parsedRequest.headers[('Content-Length').toUpperCase()] = body.length;
          parsedRequest.headers[('Date').toUpperCase()] = (new Date()).toUTCString();
          //content-type
          parsedRequest.headers[('Content-Type').toUpperCase()]=setContentType(parsed.getHeader('Accept'))
          //añadimos los headers que vengan como parámetro
          const keys = Object.keys(headers);
          keys.forEach((key, index) => {
            parsedRequest.headers[(key).toUpperCase()] = headers[key];
          });
          //armamos la respuesta 
          const httpResponse = composeHttpResponse(statusCode, parsedRequest.headers, body);
          socket.write(httpResponse);
          socket.destroy();
        }
      });
    });
    socket.setTimeout(1000);
    socket.on('timeout', () => {
      console.log('socket timeout');
      socket.end();
    });

    socket.on("end", () => {
      buffer.join();
      console.log("connection ended");
    });
  });
  return {
    listen: (portNumber) => {
      server.listen(portNumber);
    },
    close: () => {
      server.close();
    }
  };
};

module.exports = createServer;

//valores aceptados
const valuesToRespond = ['*/*','text/xml','img/*','img/jpg','text/html','text/css','img/png','video/mp4','video/webm'];
//regresara todos los valores que sean aceptados
const setContentType=((accept)=>{
  let content=''
  let cont=0
  const values=verifyAccepts(splitText(accept,','));
  if(values.length>0)
  values.forEach(element=>{
    if(cont==0)
      content+=`${element}`
    else
      content+=`,${element}`
    cont++;
  })
  return content
})
const splitText=((value,separador) => {
  if(value.includes(separador)){
    return value.split(separador)
  }else 
    return [value]
});
const verifyAccepts=(arrayAccepts)=>{
  let contentTypeResponse=[]
  arrayAccepts.forEach(elem=>{
      if(valuesToRespond.includes(elem))
        contentTypeResponse.push(elem) 
  });
  return contentTypeResponse;
}

